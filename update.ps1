# Find the generated bundled assets of your Material version
# and insert it into `codelab/base.html`

$savepwd=$pwd
$info=(pip show mkdocs-material)
$loc= $info -match '(Location: )(.*)' -replace 'Location: ',''
$v= $info -match '(Version: )(\S*)' -replace 'Version: ',''
$dir="$loc\mkdocs_material-$v.dist-info\RECORD"
#notepad $dir
$CSS_BUNDLE= Select-String -Path $dir -Pattern '(material\/)(?<n>.*main\..*\.min.css)(,)'
$PAL_BUNDLE= Select-String -Path $dir -Pattern '(material\/)(?<n>.*palette\..*\.min.css)(,)'
$SW_BUNDLE= Select-String -Path $dir -Pattern '(material\/)(?<n>.*search\..*\.min.js)(,)'
$JS_BUNDLE= Select-String -Path $dir -Pattern '(material\/)(?<n>.*bundle\..*\.min.js)(,)'

$CSS_BUNDLE= ($CSS_BUNDLE.Matches.Groups | Where-Object {$_.Name -eq 'n'}).Value
$PAL_BUNDLE= ($PAL_BUNDLE.Matches.Groups | Where-Object {$_.Name -eq 'n'}).Value
$SW_BUNDLE= ($SW_BUNDLE.Matches.Groups | Where-Object {$_.Name -eq 'n'}).Value
$JS_BUNDLE= ($JS_BUNDLE.Matches.Groups | Where-Object {$_.Name -eq 'n'}).Value
echo $CSS_BUNDLE
echo $PAL_BUNDLE
echo $SW_BUNDLE
echo $JS_BUNDLE
$template='codelab/base-custom.html'
cd $PSScriptRoot
$c = Get-Content -Raw -Encoding 'UTF8NoBOM' -Path $template

$outPut= $c -replace 'assets\/.*/bundle\.\S*\.min\.js',$JS_BUNDLE `
    -replace 'assets\/.*/main\.\S*\.min\.css', $CSS_BUNDLE `
    -replace 'assets\/.*/palette\.\S*\.min\.css', $PAL_BUNDLE `
    -replace 'assets\/.*/search\.\S*\.min\.js', $SW_BUNDLE
Set-Content -Path 'codelab/base.html' -Value $outPut
cd $savepwd

echo "Updated codelab/base.html"