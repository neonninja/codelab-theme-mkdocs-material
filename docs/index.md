---
hide:
  - toc
---

A derivation of [Hugo Codelab Themes](https://hugothemesfree.com/tag/codelabs/) (which are copies of [Google Codelabs](https://github.com/googlecodelabs/tools)), which can integrate and look consistent with [MkDocs Material Theme](https://squidfunk.github.io/mkdocs-material/) in some multi-repo build scheme.

Take a look at [Setup and Usage](./guide.md).

[Right-to-Left](./guide/rtl.html).